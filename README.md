# ros-chroot-creator

Run ROS in an Ubuntu chroot via schroot+debootstrap

Make sure you cloned with `--recursive`! We need make-ubuntu-chroot for this.

You can create a ROS chroot like so:

`ros-chroot-creator --ros1 --ros2 foxy focal`

This will install the ROS1 distribution corresponding to Ubuntu 20.04 Focal, as well
as the ROS2 foxy distribution, which is compatible with Ubuntu 20.04.

Use `--help` for a full list.

## Why is this useful?

Obviously, you can run ROS on non-Ubuntu systems much more easily by using an Ubuntu chroot.
And, even if you're on Ubuntu, it helps to have the extra isolation of a chroot -
if you mess up your ROS distribution, you can just nuke the chroot and build a new one.

## Why not containers?

Containers are also a fine choice for running ROS on non-Ubuntu systems, but
chroots have less overhead and also in my personal opinion integrate better
with the main OS (schroot, for example, mounts your home directory with configs
and everything).

