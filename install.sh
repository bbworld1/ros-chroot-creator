#!/bin/sh
# Installs dependencies for ros-chroot-creator.

cd make-ubuntu-chroot
./install.sh
cd ..
sudo install -m 755 ros-chroot-creator /usr/local/bin
